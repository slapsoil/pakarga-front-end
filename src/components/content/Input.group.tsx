export const InputGroup = () => {
  return (
    <>
      <div
        className="container-last"
        style={{
          display: "flex",
          justifyContent: "space-around",
          marginBottom: "15px",
        }}
      >
        <div className="col-md-4">
          <label className="form-label">Helper</label>
          <select id="inputState" className="form-select">
            <option>0</option>
            <option>1</option>
            <option>2</option>
            <option>3</option>
            <option>4</option>
            <option>5</option>
          </select>
        </div>
        <div className="col-sm-3" style={{ width: "33%", alignSelf: "end" }}>
          <label className="visually-hidden">Rate</label>
          <div className="input-group">
            <div className="input-group-text">Rate:</div>
            <input
              type="text"
              className="form-control"
              id="specificSizeInputGroupUsername"
              placeholder="PHP"
            />
          </div>
        </div>
      </div>
      <div
        className="weight-input-container"
        style={{ width: "100%", height: "40px", marginBottom: "15px" }}
      >
        <div className="col-sm-3" style={{ width: "50%", margin: "auto" }}>
          <label className="visually-hidden">Weight</label>
          <div className="input-group">
            <div className="input-group-text">Total Item Weight:</div>
            <input
              type="text"
              className="form-control"
              id="specificSizeInputGroupUsername"
              placeholder="kilogram"
            />
          </div>
        </div>
      </div>
    </>
  )
}
