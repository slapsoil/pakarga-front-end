export const BookAddress = () => {
  return (
    <>
      <div className="col-md-6" style={{ width: "100%" }}>
        <label className="form-label">Enter Pick Up Address</label>
        <input
          type="text"
          className="form-control"
          id="validationCustom03"
          required
        />
      </div>

      <div className="col-md-6" style={{ width: "100%", marginBottom: "25px" }}>
        <label className="form-label">Enter Delivery Address</label>
        <input
          type="text"
          className="form-control"
          id="validationCustom03"
          required
        />
      </div>
      <hr />
    </>
  )
}
