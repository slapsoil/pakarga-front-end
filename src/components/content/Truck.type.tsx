export const TruckType = () => {
  return (
    <>
      <h1 className="display-6">Truck Type</h1>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck1"
        />
        <label className="form-check-label">10w Close Van</label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck2"
        />
        <label className="form-check-label">10w Drop Side</label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck1"
        />
        <label className="form-check-label">8w Closed Van</label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck2"
        />
        <label className="form-check-label">6w Closed Van</label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck1"
        />
        <label className="form-check-label">6w Drop Side</label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck2"
        />
        <label className="form-check-label">4w Closed Van</label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck1"
        />
        <label className="form-check-label">ELF Truck</label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck2"
        />
        <label className="form-check-label">Reefer Truck</label>
      </div>
      <div className="form-check">
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck1"
        />
        <label className="form-check-label">Mini Dump Truck</label>
      </div>
      <div className="form-check" style={{ marginBottom: "25px" }}>
        <input
          className="form-check-input"
          type="checkbox"
          value=""
          id="defaultCheck2"
        />
        <label className="form-check-label">Multicab</label>
      </div>
      <hr />
    </>
  )
}
