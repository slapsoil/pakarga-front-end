import logo from "/home/alvin/Desktop/pakarga-frontend/book-with-pakarga/src/images/logo.png"

export const Logo = () => {
  return (
    <>
      <div
        className="logo-container"
        style={{
          width: "224px",
          height: "50px",
          marginTop: "15px",
        }}
      >
        <img src={logo} alt="logo" style={{ width: "224px", height: "50px" }} />
      </div>
    </>
  )
}
