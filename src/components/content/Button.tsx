export const Button = () => {
  return (
    <>
      <div
        className="col-12 "
        style={{
          display: "flex",
          justifyContent: "center",
          height: "48px",
        }}
      >
        <button
          className="btn btn-primary"
          type="submit"
          style={{ width: "50%" }}
        >
          Go
        </button>
      </div>
    </>
  )
}
