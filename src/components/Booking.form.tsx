import { BookAddress } from "./content/Book.Address"
import { ServiceType } from "./content/Service.type"
import { TruckType } from "./content/Truck.type"
import { Logo } from "./content/Logo"
import { InputGroup } from "./content/Input.group"
import { Button } from "./content/Button"

export const Form = () => {
  return (
    <div
      className="container fon"
      style={{
        width: "50%",
        height: "1500px",
        backgroundColor: "gray",
        margin: "auto",
      }}
    >
      <div
        className="innerContainer"
        style={{
          height: "100%",
          width: "100%",
          display: "flex",
          flexDirection: "column",
        }}
      >
        <form
          className="row g-3 needs-validation"
          style={{
            width: "85%",
            height: "330px",
            margin: "auto",
            marginTop: "10px",
          }}
          method="POST"
        >
          <Logo />
          <BookAddress />
          <ServiceType />
          <TruckType />
          <InputGroup />
          <Button />
        </form>
      </div>
    </div>
  )
}
