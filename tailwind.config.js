module.exports = {
  content: ["./src/**/*.{js,jsx,ts,tsx}"],
  theme: {
    extend: {
      fontFamily: {
        default: ["Montserrat"],
      },
      colors: {
        blueOld: "#1A3B70ff",
        orangeOld: "#FB9B51ff",
        primary: {
          900: "#0F172A",
          800: "#1E293B",
          700: "#334155",
          600: "#475569",
          500: "#64748B",
          400: "#94A3B8",
          300: "#CBD5E1",
          200: "#E2E8F0",
          100: "#F1F5F9",
        },
        secondary: {
          900: "#7F1D1D",
          800: "#991B1B",
          700: "#B91C1C",
          600: "#DC2626",
          500: "#EF4444",
          400: "#F87171",
          300: "#FCA5A5",
          200: "#FECACA",
          100: "#FEE2E2",
        },
      },
    },
  },
  plugins: [],
};
